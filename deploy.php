<?php
namespace Deployer;

require 'recipe/symfony.php';

// Project name
set('application', 'API');

// Project repository
set('repository', 'git@gitlab.com:yawik/api.git');

// Shared files/dirs between deploys
add('shared_files', [
    'public/.htaccess',
    '.env.local'
]);
add('shared_dirs', [
    'var/log',
    'config/autoload',
    'public/static'
]);

// Writable dirs by web server
add('writable_dirs', [
    'var/cache',
    'var/log',
    'public/static'
]);

set('symfony_env', 'dev');
set('default_stage', 'prod');
set('composer_options', '{{composer_action}} --verbose --prefer-dist --no-progress --no-interaction --optimize-autoloader');


host('staging.api.yawik.org')
    ->user('yawik')
    ->stage('staging')
    ->multiplexing(false)
    ->set('deploy_path', '/var/www/api-staging');
    
host('api.yawik.org')
    ->user('yawik')
    ->stage('prod')
    ->multiplexing(false)
    ->set('deploy_path', '/var/www/api');

#after('deploy:symlink', 'cachetool:clear:opcache');

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

