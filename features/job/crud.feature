Feature: Jobs

  Scenario: Successfully create job
    Given I don't have job with title "Chef"
    And I add "Content-Type" header equal to "application/json"
    And I send a "POST" request to "/jobs" with body:
    """
    {
      "title": "Chef",
      "company": "Catering Company",
      "uriApply": "https://example.com/apply",
      "contactEmail": "apply@example.com",
      "reference": "foo bar",
      "locations": [
        {
          "streetName": "Some Street",
          "streetNumber": "401",
          "city": "Some City",
          "region": "Some Region",
          "postalCode": "1234",
          "country": "Some Country",
          "coordinates": {
            "type": "Point",
            "coordinates": [
              "123.456789",
              "123.456789"
            ]
          }
        }
      ],
      "salary": {
         "currency": "USD",
         "unit": "YEAR",
         "value": 40000
      }
    }
    """
    Then the response status code should be 201
    And the response should be in JSON
    And the JSON node "title" should be equal to "Chef"
    And the JSON node "company" should be equal to "Catering Company"
