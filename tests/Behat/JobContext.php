<?php

namespace Yawik\Tests\Behat;

use Behat\Behat\Context\Context;
use Symfony\Component\HttpKernel\KernelInterface;
use Yawik\Testing\Concern\InteractsWithJob;

class JobContext implements Context
{
    use InteractsWithJob;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @param string $title
     * @Given I don't have job with title :title
     */
    public function iDonTHaveJob(string $title)
    {
        $this->thereAreNoJob($title);
    }
}
